const express = require("express");
const router = express.Router();

const controller = require("../controller/Hub")


router.post("/user",controller.authenticateUser);


module.exports = router;