const SearchUserModel = require('../model/SearchUserModel');
const AuthenticateUserModel = require('../model/AuthenticateUserModel')

exports.authenticateUser = (req,res) => {
    console.log(`Authenticate User Login API called ${JSON.stringify(req.body)}`);
    if(!req.body){
        console.log(`Invalid payload -->`,JSON.stringify(req.body));
        res.status(403).json({
          "status" : 1,
          "message" : "Unable to authenticate user",
          "errorMessage" : "Invalid payload"
        })
        return
      }

      if(!((req.body.username) && (req.body.password))){
        console.log(`Please enter username and password to proceed -->`,JSON.stringify(req.body));
        res.status(403).json({
          "status" : 1,
          "message" : "Unable to authenticate user",
          "errorMessage" : "Please enter username and password to proceed"
        })
        return
      }

    const {username, password} = req.body
    console.log("Username:", username, " Password:",password);

    // Specify search parameters
    const opts = {
        filter: `cn=${username}`,//'(|(cn=Bijayini)(uid=1)(sn=Sahoo))',//'(&(cn=Bijayini)(uid=2))',//'(uid=2)',//'(uid=*)',//'(cn=Bijayini)',//'(cn=*)',//'objectClass=*',
        scope: 'sub',
        attributes: ['sn', 'cn', 'userPassword'],
      };
    const searchBase = 'ou=users,ou=system';

    SearchUserModel.SearchUser(searchBase,opts,SearchUserCallback => {
        // console.log("/////");
        console.log("SearchUserCallback <<", SearchUserCallback);

        if(SearchUserCallback.status == 0) {
            console.log(`The user "${username}" is present in the directory`);
            
            let user_attributes = {
                "dn" : `cn=${username},ou=users,ou=system`,
                "UserPassword" : password,
                "UserName" : username
            }
            
            AuthenticateUserModel.authenticateUser(user_attributes,authenticateUserCallback => {
                if(authenticateUserCallback.status == 0) {
                    console.log(`User "${username}" logged in successfully`);
                    res.json("User logged in successfully").status(200)
                }
                else {
                    res.json("Password is invalid, please try again!").status(401)
                }

            })
            // res.json("The entered field username and password are correct").status(200)
            
        }
        else{
            console.log(`The user "${username}" is not present in the directory`);
            res.json(`The user ${username} is not present in the directory`).status(404)
        }

    })


}