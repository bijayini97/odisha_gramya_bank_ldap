const ldap = require('ldapjs');

const ldapServerConfig = require('./config/ldap_config').development

// Specify LDAP server details
const client = ldap.createClient({
    url: 'ldap://127.0.0.1:10389'
  });

// Bind to the LDAP server with credentials
const bindCredentials = {
    bindDN: ldapServerConfig.bindDN,
    bindPassword: ldapServerConfig.bindPassword,
  };

client.bind(bindCredentials.bindDN, bindCredentials.bindPassword, (err) => {
    
    if(err){
        console.log("Error in connecting to LDAP server",err);

    } else{
        console.log("Successfully connected to ldap server");
    }
  });

module.exports = {
    ldapClient : client
}