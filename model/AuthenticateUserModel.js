const client = require('../env').ldapClient

module.exports = class AuthenticateUserModel {

    static authenticateUser(user_attributes,callback) {
        // console.log("User_attributes <<",user_attributes);
        console.log(`will now authenticate user "${user_attributes.UserName}"`);

        client.bind(user_attributes.dn, user_attributes.UserPassword, (err) => {
        
            if(err){
                console.log("The entered password is incorrect")
                console.log("Error from LDAP server << ",err.message);
                callback({
                    "status": 1,
                    "message": "Invalid Password"
                })
    
            } else{
                console.log("The entered field username and password are correct");
                callback({
                    "status": 0,
                    "message": "The entered field username and password are correct"
                })
            }
          });
        
    }
}