const client = require('../env').ldapClient

module.exports = class SearchUserModel {

    static SearchUser(searchBase,opts,callback) {
        console.log(`will check if the user ${JSON.stringify(opts.filter)} is present in the directory or not`);
        console.log("The searching parameters are -- ",JSON.stringify(opts));

        client.search(searchBase, opts, (err, res) => {
            
            if(err) {
                console.log("Error in search ",err);
                callback({
                    "status": 1,
                    "message": "Error in search"
                })
            }
            else {
                let is_true = false;
    
                res.on('searchEntry', (entry) => {
                    // console.log('entry: ' + JSON.stringify(entry));
                    is_true = (entry.pojo) ? true : false
                    // console.log("is_true --",is_true);
                    
                    console.log('Result <<< ' + JSON.stringify(entry.pojo));
                    let values = (entry.pojo.attributes[0]) ? (entry.pojo.attributes[0].values[0]) : null
                    let data = (entry.pojo.attributes[0]) ? (entry.pojo.attributes[2].values[0]) : null
                    // console.log("User SSHA hash Password -- ",data)
                    // console.log("User SSHA hash Password -- ",typeof(data))
                    // console.log('Attribute values: ' + JSON.stringify(values));
                   
                });
    
                res.on('error', (err) => {
                    console.error('error: ' + err.message);
                });
    
                res.on('end', (result) => {
                    // console.log("is_true <<<",is_true);
                    console.log('status: ' + result.status);
                    if((is_true)){
                        callback({
                            "status": 0,
                            "message": "The user is present in directory",
                        })
                    }
                    
                    else{
                        callback({
                            "status": 1,
                            "message": "User is not present in directory"
                        })
                    }
                    
                });
            }
          
        });
    }
}