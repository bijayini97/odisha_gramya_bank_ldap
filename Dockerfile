# Use the official lightweight Node.js 12 image.
# https://hub.docker.com/_/node
FROM node:18.12.1-slim

# Create and change to the app directory.
WORKDIR /usr/src/app

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure both package.json AND package-lock.json are copied.
# Copying this separately prevents re-running npm install on every code change.
COPY package*.json ./

# Install production dependencies.
RUN npm install --only=production

# Copy local code to the container image.
COPY . ./

# Run the web service on container startup.
CMD [ "npm", "start" ]

# gcloud builds submit --tag gcr.io/creditapp-29bf2/OGB_ldap_Prod
# gcloud run deploy --image gcr.io/creditapp-29bf2/OGB_ldap_Prod --platform managed

# gcloud builds submit --tag gcr.io/iserveustaging/OGB_ldap_Staging
# gcloud run deploy --image gcr.io/iserveustaging/OGB_ldap_Staging --platform managed
